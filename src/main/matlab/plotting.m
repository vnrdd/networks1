clear;
close all;

ideal = load('/Users/ivanrud/Desktop/networks1/res/test.txt');
pe_up = ideal(:,2);
pe_acc = ideal(:, 3);
p = ideal(:, 1);

figure(1);
plot(p, pe_up);
hold on;
grid on;

plot(p, pe_acc);
hold on;
grid on;

legend("Error probability upper bound", "Error probability accuracy value", "Location", "northwest");

%%
ideal = load('/Users/ivanrud/Desktop/networks1/res/p01.txt');
pe_up = ideal(:,2);
pe_acc = ideal(:, 3);
l = ideal(:, 1);

ideal = load('/Users/ivanrud/Desktop/networks1/res/minD.txt');
y = ideal(:, 2);

figure(2);
subplot(3,1,1);
plot(l, pe_up);
hold on;
grid on;

plot(l, pe_acc);
hold on;
grid on;
title("p = 0.1");

plot(l, y);
hold on;
grid on;

legend("Error probability upper bound", "Error probability accuracy value", "Location", "northwest");

%%
ideal = load('/Users/ivanrud/Desktop/networks1/res/p02.txt');
pe_up = ideal(:,2);
pe_acc = ideal(:, 3);
l = ideal(:, 1);

subplot(3,1,2);
plot(l, pe_up);
hold on;
grid on;

plot(l, pe_acc);
hold on;
grid on;
title("p = 0.2");

plot(l, y);
hold on;
grid on;

legend("Error probability upper bound", "Error probability accuracy value", "Location", "northwest");

%%
ideal = load('/Users/ivanrud/Desktop/networks1/res/p03.txt');
pe_up = ideal(:,2);
pe_acc = ideal(:, 3);
l = ideal(:, 1);

subplot(3,1,3);
plot(l, pe_up);
hold on;
grid on;

plot(l, pe_acc);
hold on;
grid on;
title("p = 0.3");

plot(l, y);
hold on;
grid on;

legend("Error probability upper bound", "Error probability accuracy value", "Location", "northwest");

