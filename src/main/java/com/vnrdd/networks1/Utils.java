package com.vnrdd.networks1;

import java.util.List;

public class Utils {
    public static int findMax(List<Integer> sample){
        int max = 0;

        for(Integer elem : sample)
            max = elem >= max ? elem : max;

        return max;
    }

    public static int[] stringToIntArray(String src) {
        int[] dst = new int[src.length()];

        for(int i = 0; i < src.length(); ++i)
            dst[dst.length - i - 1] = Integer.parseInt(String.valueOf(src.charAt(i)));

        return dst;
    }
}
