package com.vnrdd.networks1;

public class Coder {
    public static Polynom produce(Polynom g_x, Polynom m) {
        String multiplier = "x^" + g_x.getDegree();
        Polynom tmp = new Polynom(Polynom.multiply(m, new Polynom(multiplier)));

        Polynom c_x = Polynom.divide(tmp, g_x);;

        return new Polynom(Polynom.sum(tmp, c_x));
    }
}
