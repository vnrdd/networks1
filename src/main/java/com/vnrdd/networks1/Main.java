package com.vnrdd.networks1;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String g_xBuf = "";
        int l, eps;

        System.out.print("Enter g(x) [ex: x^4+x]: ");
        g_xBuf = scanner.nextLine();
        Polynom g_x = new Polynom(g_xBuf);

        System.out.print("Enter l [length of the sequence]: ");
        l = scanner.nextInt();

        System.out.print("Enter epsilon [accuracy]: ");
        eps = scanner.nextInt();

        //
        System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");

        // CODER PROCESS
        Polynom m = new Polynom(l, true);

        Polynom a_x = Coder.produce(g_x, m);
        int n = g_x.getDegree() + m.getDegree();

        // COUNTING UPPER BOUND ERROR PROBABILITY
//        System.out.print("Enter decoder error probability: ");
//        double p = scanner.nextDouble();

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("res/test.txt"));
        }
        catch(Exception e){
            e.printStackTrace();
        }

        for(double p = 0.0; p <= 1.0; p+=0.1){
            double Pe_up = Analysis.Pe_up(g_x.getDegree(), p, n);
            double Pe_acc = Analysis.Pe_acc(g_x, l, g_x.getDegree(), p, n);

            try {
                bw.write(Double.toString(p) + " ");
                bw.write(Double.toString(Pe_up) + " ");
                bw.write(Double.toString(Pe_acc) + "\n");
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        try {
            bw.close();
        }

        catch(Exception e){
            e.printStackTrace();
        }

        //double Pe_up = Analysis.Pe_up(g_x.getDegree(), p, n);
  //      Pe_up = Maths.round(Pe_up, eps);

//        System.out.println("Error probability upper bound: " + Pe_up);

        //double Pe_acc = Analysis.Pe_acc(g_x, l, g_x.getDegree(), p, n);

    }
}
