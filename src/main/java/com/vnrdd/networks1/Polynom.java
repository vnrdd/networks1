package com.vnrdd.networks1;

import java.util.ArrayList;
import java.util.Random;

public class Polynom {
    private int[] coefs;

    public Polynom(int degree, boolean random) {
        coefs = new int[degree];

        if (random) {
            coefs[0] = 1;
            for (int i = 1; i < degree; ++i)
                coefs[i] = (new Random()).nextInt(2);
        }
    }

    public Polynom(Polynom copy) {
        this.coefs = new int[copy.coefs.length];
        System.arraycopy(copy.coefs, 0, this.coefs, 0, copy.coefs.length);
    }

    public Polynom(int[] coefs) {
        this.coefs = new int[coefs.length];
        System.arraycopy(coefs, 0, this.coefs, 0, coefs.length);
    }

    public Polynom(String source) {
        if (source == null)
            throw new NullPointerException();

        if (source.equals(""))
            throw new IllegalArgumentException();

        source = source.replace("-", "+");
        source = source.replace(" ", "");
        String[] split = source.split("\\+");

        coefs = new int[Utils.findMax(new ArrayList<Integer>() {
            {
                for (String s : split) {
                    if (s.length() == 3)
                        add(Integer.parseInt(String.valueOf(s.charAt(2))));
                    else if (s.equals("x"))
                        add(1);
                    else if (s.length() == 1)
                        add(0);
                }
            }
        }) + 1];

        for (String s : split) {
            if (s.contains("^")) {
                int index = Integer.parseInt(s.substring(2, 3));
                coefs[index] = 1;
            } else if (s.equals("x"))
                coefs[1] = 1;

            else if (s.length() == 1) {
                coefs[0] = 1;
            }
        }
    }

    public int getDegree() {
        return coefs.length - 1;
    }

    private void reduceDegree() {
        int reduceDepth = 0;
        for (int i = getDegree(); coefs[i] != 1; --i) {
            ++reduceDepth;

            if (i == 0)
                break;
        }

        int newDeg = getDegree() - reduceDepth;
        int[] buf = new int[newDeg + 1];
        System.arraycopy(coefs, 0, buf, 0, newDeg + 1);

        coefs = new int[newDeg + 1];
        System.arraycopy(buf, 0, coefs, 0, newDeg + 1);
    }

    private void normalize() {
        for (int i = 0; i < getDegree(); ++i) {
            coefs[i] %= 2;
            if (coefs[i] == -1)
                coefs[i] = 1;
        }
    }

    public static Polynom multiply(Polynom p1, Polynom p2) {
        Polynom res = new Polynom(p1.getDegree() + p2.getDegree() + 1, false);

        for (int i = 0; i <= p1.getDegree(); ++i) {
            for (int j = 0; j <= p2.getDegree(); ++j)
                res.coefs[i + j] += p1.coefs[i] * p2.coefs[j];
        }

        res.normalize();

        return res;
    }

    public static Polynom divide(Polynom p1, Polynom p2) {
        int resDeg = p1.getDegree() - p2.getDegree() + 1;
        Polynom res = new Polynom(resDeg, false);
        Polynom remainder = new Polynom(p1);

        if (p1.getDegree() == p2.getDegree()) {
            res.coefs[0] = 1;
            for (int i = 0; i <= remainder.getDegree(); ++i) {
                remainder.coefs[i] = p1.coefs[i] - p2.coefs[i];
            }
        }

        else {
            for (int i = 0; i < resDeg; ++i) {
                res.coefs[resDeg - i - 1] = remainder.coefs[remainder.getDegree() - i]
                        / p2.coefs[p2.getDegree()];
                for (int j = 0; j <= p2.getDegree(); ++j) {
                    remainder.coefs[remainder.getDegree() - j - i] -=
                            p2.coefs[p2.getDegree() - j] * res.coefs[resDeg - i - 1];
                }
            }
        }

        remainder.normalize();
        remainder.reduceDegree();

        return remainder;
    }

    public static Polynom sum(Polynom p1, Polynom p2) {
        Polynom res = new Polynom(p1.getDegree() > p2.getDegree() ? p1 : p2);

        int minSize = Math.min(p1.getDegree(), p2.getDegree());
        for (int i = 0; i <= minSize; ++i)
            res.coefs[i] = (p1.coefs[i] + p2.coefs[i]) % 2;

        res.normalize();
        res.reduceDegree();
        return res;
    }

    public void printVector() {
        if (getDegree() == -1) {
            System.out.println("0");
            return;
        }
        for (int i = coefs.length - 1; i >= 0; --i)
            System.out.print(coefs[i]);
        System.out.println("");
    }

    public int[] getVector() {
        return coefs;
    }

    public void printPolynom() {

        if (getDegree() == -1 || (getDegree() == 0 && coefs[0] == 0)) {
            System.out.println("0");
            return;
        }

        StringBuilder sb = new StringBuilder();
        for (int i = coefs.length - 1; i >= 0; --i) {
            if (coefs[i] == 1) {
                if (i > 1) {
                    sb.append("x^").append(i).append("+");
                } else if (i == 1) {
                    sb.append("x+");
                } else {
                    sb.append(coefs[0]);
                }
            }
        }

        if (getDegree() != 0 && sb.charAt(sb.length() - 1) == '+')
            sb.deleteCharAt(sb.length() - 1);

        System.out.println(sb.toString());
    }
}
