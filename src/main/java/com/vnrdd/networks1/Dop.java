package com.vnrdd.networks1;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Dop {
    public static void calculateAndWrite(Polynom g_x, double p, String filename) {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(filename));
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int l = 1; l < 10; ++l) {
            Polynom m = new Polynom(l, true);
            Polynom a_x = Coder.produce(g_x, m);
            int n = g_x.getDegree() + m.getDegree();

            double Pe_up = Analysis.Pe_up(g_x.getDegree(), p, n);
            double Pe_acc = Analysis.Pe_acc(g_x, l, g_x.getDegree(), p, n);

            try {
                bw.write(l + " ");
                bw.write(Double.toString(Pe_up) + " ");
                bw.write(Double.toString(Pe_acc) + "\n");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {

        // INPUTTING
        Scanner scanner = new Scanner(System.in);

        String g_xBuf = "";

        System.out.print("Enter g(x) [ex: x^4+x]: ");
        g_xBuf = scanner.nextLine();
        Polynom g_x = new Polynom(g_xBuf);
        //

        // p = 0.1;
        double p = 0.1;
        calculateAndWrite(g_x, p, "res/p01.txt");

        // p = 0.2
        p = 0.2;
        calculateAndWrite(g_x, p, "res/p02.txt");

        // p = 0.3
        p = 0.3;
        calculateAndWrite(g_x, p, "res/p03.txt");

        // Min d of l
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("res/minD1.txt"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        int[] buf = null;
        for(int l = 1; l < 10; ++l) {
            int n = g_x.getDegree() + l - 1;
            List<int[]> A = new ArrayList<>();
            int minW = Integer.MAX_VALUE;
            for (int i = 1; i < Math.pow(2, l); ++i) {
                Polynom m1 = new Polynom(Utils.stringToIntArray(Integer.toBinaryString(i)));
                int[] res = Coder.produce(g_x, m1).getVector();
                A.add(res);
                if(Maths.vectorWeight(res) < minW) {
                    buf = new int[res.length];
                    minW = Maths.vectorWeight(res);
                    System.arraycopy(res, 0, buf, 0, res.length);
                }
            }

            System.out.println(l + " " + Arrays.toString(buf));

            bw.write(l + " " + minW + "\n");
        }
            bw.close();
    }
}
