package com.vnrdd.networks1;

import java.util.ArrayList;
import java.util.List;

public class Analysis {
    public static double Pe_up(int d, double p, int n) {
        double Pe = 0.0;

        for (int i = 0; i <= d-1; ++i)
            Pe += (Maths.C(i, n) * Math.pow(p, i) * Math.pow(1 - p, n - i));

        double res = 1 - Pe;
        if (res > 1)
            res = 1.0;

        return res;
    }

    public static double Pe_acc(Polynom g_x, int k, int d, double p, int n) {
        List<int[]> A = new ArrayList<>();
        int minW = 150;

        for (int i = 1; i < Math.pow(2, k); ++i) {
            Polynom m = new Polynom(Utils.stringToIntArray(Integer.toBinaryString(i)));
            int[] res = Coder.produce(g_x, m).getVector();
            int[] buf = new int[n+1];
            System.arraycopy(res, 0, buf, 0, res.length);
            A.add(buf);
        }


        int[] freqs = new int[n+1];
        for (int[] sample : A) {
            freqs[Maths.vectorWeight(sample)-1]++;
        }

        double Pe = 0.0;
        for (int i = d; i <= n; ++i) {
            Pe += (freqs[i] * Math.pow(p, i) * Math.pow(1 - p, n - i));
        }

        return Pe;
    }
}
