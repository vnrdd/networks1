package com.vnrdd.networks1;

import java.text.DecimalFormat;
import java.util.List;

public class Maths {
    public static int C(int k, int n) {
        return factorial(n) / (factorial(k) * factorial(n - k));
    }

    public static int factorial(int a) {
        int res = 1;

        for (int i = 2; i <= a; ++i)
            res *= i;

        return res;
    }

    public static double round(double num, int eps) {
        DecimalFormat df = null;

        StringBuilder sb = new StringBuilder("#.");

        sb.append("#".repeat(eps));

        df = new DecimalFormat(sb.toString());
        String res = (df.format(num)).replace(",", ".");

        return Double.parseDouble(res);
    }

    public static int vectorWeight(int[] vector) {
        int sum = 0;
        for (int i : vector)
            sum += i;

        return sum;
    }

    public static int countD(int[] first, int[] second) {
        int d = 0;

        int n = Math.max(first.length, second.length);
        int[] buf1 = new int[n];
        int[] buf2 = new int[n];

        System.arraycopy(first, 0, buf1, 0, first.length);
        System.arraycopy(second, 0, buf2, 0, second.length);

        for(int i = 0; i < first.length; ++i) {
            if(buf1[i] != buf2[i])
                ++d;
        }

        return d;
    }

    public static int countMinD(List<int[]> words) {
        int minD = Integer.MAX_VALUE;
        int n = words.get(words.size() - 1).length;

        for(int i = 0; i < words.size(); ++i) {
            for(int k = 0; k < words.size(); ++k) {
                if(k == i)
                    continue;
                int d = countD(words.get(i), words.get(k));
                if(d < minD)
                    minD = d;
            }
        }

        return minD;
    }
}
